interface Rectangle {
    width: number;
    height: number;
}
interface colorRectangle extends Rectangle {
    color: string;
}
const rec:Rectangle = {
    width: 20,
    height: 10
}
console.log(rec);

const cra:colorRectangle = {
    width: 20,
    height: 10,
    color: "red"
}

console.log(cra);