let w:unknown = 1;
w = "string";
w = {
    runANonExistentMethod: () => {
        console.log("I am the Flash!!");
    }
} as {runANonExistentMethod:() => void}

if(typeof w == 'object' && w!==null) {
    (w as {runANonExistentMethod: Function}).runANonExistentMethod();
}